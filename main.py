def main():
    mass = [1, 5, 8, 3, 2, 4, 6, 9, 7]
    introsort(mass)
    print(str(mass))


def introsort(mass):
    maxdep = (len(mass).bit_length() - 1) * 2
    intro(mass, 0, len(mass), maxdep)


def intro(mass, start, end, maxdep):
    if end - start <= 1:
        return
    elif maxdep == 0:
        heort(mass, start, end)
    else:
        p = parti(mass, start, end)
        intro(mass, start, p + 1, maxdep - 1)
        intro(mass, p + 1, end, maxdep - 1)


def parti(mass, start, end):
    pivot = mass[start]
    i = start - 1
    j = end

    while True:
        i = i + 1
        while mass[i] < pivot:
            i = i + 1
        j = j - 1
        while mass[j] > pivot:
            j = j - 1

        if i >= j:
            return j

        swapsort(mass, i, j)


def swapsort(mass, i, j):
    mass[i], mass[j] = mass[j], mass[i]


def heort(mass, start, end):
    max(mass, start, end)
    for i in range(end - 1, start, -1):
        swapsort(mass, start, i)
        ify(mass, index=0, start=start, end=i)


def max(mass, start, end):
    index = end - start - 1 // 2
    while index >= 0:
        ify(mass, index, start, end)
        index = index - 1


def ify(mass, index, start, end):
    size = end - start
    if (2 * index + 1 < size and mass[start + 2 * index + 1] > mass[start + index]):
        lar = 2 * index + 1
    else:
        lar = index
    if (2 * index + 2 < size and mass[start + 2 * index + 2] > mass[start + lar]):
        lar = 2 * index + 2
    if lar != index:
        swapsort(mass, start + lar, start + index)
        ify(mass, lar, start, end)


main()
